# Log de Atividades

*Lindeberg Pessoa Leite*
*Aluno regular de doutorado do PPGI - Universidade de Brasília*

## Semana do dia 20/04/20 ao dia 24/04/20

Finalizei o curso Introduction to openshift application DO101 

Implementação de modelo para reconhecer dígitos de 0 a 9 (Fastai2)

Estudo livro Diniz - capítulo 2 

Estudo do capítulo 1 do livro de deep learing - Michael Nielsen  - http://neuralnetworksanddeeplearning.com/chap1.html


## Semana do dia 13/04/20 ao dia 17/04/20

Comecei o curso Introduction to openshift application DO101 

Assisti aula do curso fastai de deep learning na prática. Subi para produção uma classificador de urso, conforme atividade da lição 2

Resolução de questões de processamento digital de sinal  (capítulo 1 - Livro Diniz)

Início da leitura do artigo End-to-End Neural Entity Linking


## Semana do dia 06/04/20 ao dia 10/04/20

Estudando os artigos: https://www.kaggle.com/kenshoresearch/kdwd-wikidata-introduction e https://www.kaggle.com/kenshoresearch/kdwd-wikipedia-introduction

Resolução de questões de processamento digital de sinal 

Finalização do capítulo 1 do curso de Deep Learing na prática e início do capítulo 2 (prof Pierre e prof.Jeremy)


## Semana do dia 30/03/20 ao dia 03/04/20

Leitura do artigo: https://medium.com/analytics-vidhya/entity-linking-a-primary-nlp-task-for-information-extraction-22f9d4b90aa8 e https://towardsdatascience.com/named-entity-disambiguation-boosted-with-knowledge-graphs-4a93a94381ef


## Semana do dia 23/03/20 ao dia 27/03/20

Revisão de métodos para detecção de lavagem de dinheiro (artigo: Data Mining Techniques for Anti Money Laundering)

Adaptação ao trabalho remoto

Estudo da disciplina Processamento digital de Sinal. Livro do Diniz (cap 1)

## Semana do dia 16/03/20 ao dia 20/03/20

Estudo das disciplinas Processamento digital de Sinal e Teoria da Computação

## Semana do dia 09/03/20 ao dia 13/03/20

Fiz alguns testes no projeto do Hugo Honda, aluno do professor Vidal. O projeto recupera conteúdo do DOU e faz um processamento de linguagem natural para dectação de fraude (https://github.com/hugohonda/deep-vacuity ).  
Procurei por ferramentas para facilitar o processo de anotação: Doccano ou WebAnno, 


## Semana do dia 02/03/20 ao dia 06/03/20

Estive estudando formas de extração do conteúdo de texto puro para uma representação semiestruturada (penso que o NLP deva executar em cima dessa últma representação). 
Basicamente, estive avaliando duas técnicas de information extraction: 1) Rule-Based e 2)  Supervised text segmentation. 
1) Rule-Based - Esse método ganha força quando combinado com NER. Desse modo, é  possível treinar o modelo para identificar entidades do tipo seção, tópico, decreto, atos, etc. 
Uma vez que o método entende esses tipos, o processo de parsing e extração de sentenças são facilitados.  Segue referência: https://medium.com/@ashiqgiga07/rule-based-matching-with-spacy-295b76ca2b68.
2) Supervised text segmentation – Segmentação de texto supervisionada. Li este dois artigos: https://arxiv.org/pdf/1803.09337.pdf 
e https://medium.com/illuin/https-medium-com-illuin-nested-bi-lstm-for-supervised-document-parsing-1e01d0330920


## Semana do dia 18/02/20 ao dia 28/02/20

* Viagem de férias. Pouca produção; apenas leitura de alguns textos sobre Information extraction  


## Semana do dia 10/02/20 ao dia 14/02/20

* [Finalização da leitura do artigo DeepWalk](https://gitlab.com/UnBVision/lindeberg/-/blob/master/artigos-lidos/deepwalk.pdf)
* Escrita de programa em Pyhon para processamento de dados do Sistema de Investigação Bancária (Simba) (3 milhões e 27 milhões de registros)



## Semana do dia 03/02/20 ao dia 07/02/20

* [Finalização do artigo Node2Vec](artigos-lidos/node2vec__Scalable_Feature_Learning_for_Networks.pdf)
* [Início da leitura do artigo DeepWalk](https://gitlab.com/UnBVision/lindeberg/-/blob/master/artigos-lidos/deepwalk.pdf)
* Análise inicial de duas bases do Simba para o projeto (3 milhões e 27 milhões de registros)



## Semana do dia 27/01/20 ao dia 31/01/20

* [Leitura do artigo Node2Vec](artigos-lidos/node2vec__Scalable_Feature_Learning_for_Networks.pdf)
* [Leitura sobre Geometric Deep Learning](https://towardsdatascience.com/graph-convolutional-networks-for-geometric-deep-learning-1faf17dee008)

## Semana do dia 20/01/2020 ao dia 24/01/2020

* Finalização do curso de especialização de Deep Learning (Coursera), Prof. Andrew Ng
